package v1

import (
	"github.com/gin-gonic/gin"
	controllerV1 "gitlab.com/tatangdev/minat-baca-backend/controllers/v1"
)

func AddEndpoints(baseRoute *gin.RouterGroup) {
	v1 := baseRoute.Group("/v1")

	books := v1.Group("/books")
	books.POST("/", controllerV1.CreateBook)
}
