package envvar

import (
	"fmt"
	"os"
)

const (
	AppName        = "APP_NAME"
	Environment    = "ENVIRONMENT"
	SentryDSN      = "SENTRY_DSN"
	AllowedOrigins = "ALLOWED_ORIGINS"
	HttpPort       = "HTTP_PORT"
)

func LoadEnvVar(key string, isRequired bool) (out string, err error) {
	if key == "" {
		err = fmt.Errorf("missing key")
		return
	}
	out = os.Getenv(key)
	if out == "" && isRequired {
		err = fmt.Errorf("missing ENVVAR %s", key)
		return
	}
	return
}
