package main

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/getsentry/sentry-go"
	sentrygin "github.com/getsentry/sentry-go/gin"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	routeV1 "gitlab.com/tatangdev/minat-baca-backend/routes/v1"
	"gitlab.com/tatangdev/minat-baca-backend/util/envvar"
)

func main() {
	environment, err := envvar.LoadEnvVar(envvar.Environment, true)
	if err != nil {
		log.Fatal(err)
		return
	}

	sentryDSN, err := envvar.LoadEnvVar(envvar.SentryDSN, true)
	if err != nil {
		log.Fatal(err)
		return
	}

	err = sentry.Init(sentry.ClientOptions{
		Dsn:         sentryDSN,
		Environment: environment,
	})
	if err != nil {
		log.Fatalf("sentry.Init: %s", err)
	}

	r := gin.Default()                        // Gin
	r.Use(sentrygin.New(sentrygin.Options{})) // SentryGin

	allowedOrigins, err := envvar.LoadEnvVar(envvar.AllowedOrigins, true)
	if err != nil {
		log.Fatal(err)
		return
	}

	// CORS module
	corsConfig := cors.DefaultConfig()
	corsConfig.AllowWildcard = true
	corsConfig.AllowOrigins = strings.Split(allowedOrigins, ",") // contain whitelist domain
	corsConfig.AllowHeaders = []string{"*", "user", "domain", "verb", "object", "Content-Type", "Accept"}
	corsConfig.AllowCredentials = true
	corsConfig.AddAllowMethods("OPTIONS")
	r.Use(cors.New(corsConfig))

	prefix := "/api"
	baseRoute := r.Group(prefix)

	routeV1.AddEndpoints(baseRoute)
	httpPortStr, err := envvar.LoadEnvVar(envvar.HttpPort, true)
	if err != nil {
		sentry.CaptureException(err)
		log.Fatal(err)
		return
	}
	var httpPort int64
	if httpPort, err = strconv.ParseInt(httpPortStr, 10, 64); err != nil {
		sentry.CaptureException(err)
		log.Fatal(err)
		return
	}
	err = r.Run(fmt.Sprintf(":%d", httpPort))
	if err != nil {
		sentry.CaptureException(err)
		log.Error(err)
	}
}
